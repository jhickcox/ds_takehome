feature_columns = [
    #     'query_num_guests', 'query_num_children', 'query_num_infants',
    'listing_is_new',
    'listing_instant_bookable',
    #     'listing_review_rating',
    #     'listing_review_count',
    #                       'listing_property_type',
    'listing_num_beds',
    'listing_num_bedrooms',
    'listing_num_bathrooms',
    'listing_person_capacity',
    'listing_has_pro_pictures',
    #         'listing_num_recent_reservations',
    'listing_location_rating',
    'listing_cleanliness_rating',
    'listing_checkin_rating',
    'listing_value_rating',
    'listing_communication_rating',
    'listing_accuracy_rating',
    #            'listing_num_books_90day',
    #     'listing_occupancy_rate',
    #        'listing_monthly_discount', 'listing_weekly_discount',
    #        'listing_cleaning_fee',
    #     'listing_monthly_price_factor',
    #                       'query_market_Rio de Janeiro', 'query_market_Sao Paulo',
    'listing_room_type_Entire Home',
    'listing_room_type_Private Room',
    'listing_room_type_Shared Room'
]

engineered_columns = ['occupancy_difference', 'people_per_bedroom', 'popularity',
                      'max_price_difference', 'min_price_difference'
                      ]

scaler = MinMaxScaler()


def featurize(X: pd.DataFrame, for_train: bool) -> pd.DataFrame:
    if 'listing_room_type_Private Room' not in X.columns:
        X = pd.get_dummies(X, prefix=['listing_room_type'], columns=['listing_room_type'])

    id_columns = ['id_listing', 'id_listing_int']
    id_columns_train = ['id_user', 'id_user_int', 'label_strength']
    all_columns = feature_columns + id_columns
    if for_train:
        all_columns.extend(id_columns_train)

    X_train = X

    # Fill in missing values with either the average of the other values or 0
    X_train['listing_is_new'] = X['listing_is_new'].astype('float')
    X_train['listing_person_capacity'] = X['listing_person_capacity'].astype('float')
    X_train['listing_num_recent_reservations'] = X['listing_num_recent_reservations'].astype('float')
    X_train['listing_instant_bookable'] = X['listing_instant_bookable'].astype('float')
    X_train['listing_has_pro_pictures'] = X['listing_has_pro_pictures'].fillna(0).astype('float')
    X_train['listing_review_rating'] = fill_na_with_mean(X, 'listing_review_rating', replace_zero=True)
    X_train['listing_review_count'] = X['listing_review_count'].fillna(0)
    X_train['listing_property_type'] = X['listing_property_type'].fillna(0)
    X_train['listing_num_bedrooms'] = fill_na_with_mean(X, 'listing_num_bedrooms', replace_zero=False)
    X_train['listing_num_beds'] = fill_na_with_mean(X, 'listing_num_beds', replace_zero=False)
    X_train['listing_num_bathrooms'] = pd.to_numeric(X['listing_num_bathrooms'], errors='coerce')
    X_train['listing_num_bathrooms'] = fill_na_with_mean(X_train, 'listing_num_bathrooms', replace_zero=False)
    X_train['listing_num_books_90day'] = fill_na_with_mean(X_train, 'listing_num_books_90day', replace_zero=False)
    X_train['listing_occupancy_rate'] = fill_na_with_mean(X, 'listing_occupancy_rate', replace_zero=False)
    X_train['listing_cleaning_fee'] = fill_na_with_mean(X, 'listing_cleaning_fee', replace_zero=False)
    X_train['listing_location_rating'] = X_train['listing_location_rating'].replace(-1.0,
                                                                                    X['listing_location_rating'].mean())
    X_train['listing_cleanliness_rating'] = X_train['listing_cleanliness_rating'].replace(-1.0, X[
        'listing_cleanliness_rating'].mean())
    X_train['listing_checkin_rating'] = X_train['listing_checkin_rating'].replace(-1.0,
                                                                                  X['listing_checkin_rating'].mean())
    X_train['listing_value_rating'] = X_train['listing_value_rating'].replace(-1.0, X['listing_value_rating'].mean())
    X_train['listing_communication_rating'] = X_train['listing_communication_rating'].replace(-1.0, X[
        'listing_communication_rating'].mean())
    X_train['listing_accuracy_rating'] = X_train['listing_accuracy_rating'].replace(-1.0,
                                                                                    X['listing_accuracy_rating'].mean())

    # Features to derive: difference between query people and bedrooms/capacity
    # Add popularity from listings
    # Difference between how many people will be staying and how many people can fit
    if for_train:
        scaler.fit(X_train[feature_columns])

    X_train[feature_columns] = scaler.transform(X_train[feature_columns])
    return X_train
